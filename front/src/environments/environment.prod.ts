export const environment = {
  production: true,
  apiUrlFacture: 'http://mari.api.local/SpringMVC/facture',
  apiUrlOperateur: 'http://mari.api.local/SpringMVC/operateur',
  apiUrlProduct: 'http://mari.api.local/SpringMVC/produit',
  apiUrlReglement: 'http://mari.api.local/SpringMVC/reglement',
  apiUrlSA: 'http://mari.api.local/SpringMVC/secteurActivite',
  apiUrlStock: 'http://mari.api.local/SpringMVC/stock'
};
